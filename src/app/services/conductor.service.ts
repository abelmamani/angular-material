import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Conductor } from '../models/conductor.model';
@Injectable({
  providedIn: 'root'
})
export class ConductorService {
  url: string = "http://localhost:8080/conductors";

  constructor(private http: HttpClient) { }

  getConductors(): Observable<Conductor[]>{
    return this.http.get<Conductor[]>(this.url);
  }

  createConductor(conductor: Conductor): Observable<any>{
    return this.http.post(this.url, conductor);
  }

  updateConductor(conductor: Conductor): Observable<any>{
    return this.http.put(this.url, conductor);
  }

  getConductor(id: number): Observable<Conductor>{
    return this.http.get<Conductor>(this.url+"/"+id);
  }

  deleteConductor(id: number): Observable<any>{
    return this.http.delete(this.url+"/"+id);
  }
}
