import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BusStop } from '../models/bus.stop.model';

@Injectable({
  providedIn: 'root'
})
export class BusStopService {

  url: string = "http://localhost:8080/bus_stop";

  constructor(private http: HttpClient) { }

  getBusStops(): Observable<BusStop[]>{
    return this.http.get<BusStop[]>(this.url);
  }

  createBusStop(busStop: BusStop): Observable<any>{
    return this.http.post(this.url, busStop);
  }
  
  updateBusStop(busStop: BusStop): Observable<any>{
    return this.http.put(this.url, busStop);
  }

  getBusStop(id: number): Observable<BusStop>{
    return this.http.get<BusStop>(this.url+"/"+id);
  }

  deleteBusStop(id: number): Observable<any>{
    return this.http.delete(this.url+"/"+id);
  }
}
