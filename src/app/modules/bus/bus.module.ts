import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusComponent } from './bus.component';
import { BusRoutingModule } from './bus-routing.module';
import { CreateBusComponent } from './components/create-bus/create-bus.component';
import { DeleteBusComponent } from './components/delete-bus/delete-bus.component';
import { UpdateBusComponent } from './components/update-bus/update-bus.component';
import { ListBusComponent } from './components/list-bus/list-bus.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';



@NgModule({
  declarations: [
    BusComponent,
    CreateBusComponent,
    DeleteBusComponent,
    UpdateBusComponent,
    ListBusComponent
  ],
  imports: [
    CommonModule,
    BusRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatDialogModule,
    MatInputModule
  ]
})
export class BusModule { }
