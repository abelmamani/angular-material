import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Bus } from 'src/app/models/bus.model';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-update-bus',
  templateUrl: './update-bus.component.html',
  styleUrls: ['./update-bus.component.scss']
})
export class UpdateBusComponent implements OnInit{
  @Input() id!: number;
  formBus!: FormGroup;
  constructor(private busService: BusService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<UpdateBusComponent>){
    this.formBus = this.formBuilder.group({
      id: [0, Validators.required],
      licensePlate: [undefined, Validators.required],
      brand: [undefined, Validators.required],
      model: [undefined, Validators.required],
      seats: [undefined, Validators.required]
    });
  }

  ngOnInit(): void {
    this.busService.getBus(this.id).subscribe({
      next: (res: Bus) => {
        this.formBus.patchValue(res);
      },
      error: (err) => {
        this.showSnachBar("ocurrio un error al obtener el bus!");
      }
    });
  }

  onSubmit(){
    if(this.formBus.valid){
      this.busService.updateBus(this.formBus.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnachBar(err.error ? err.error.message : "ocurrio un problema al actualizar bus!");
        }
      });
    }else{
      this.showSnachBar("complete los campos!");
    }
  }
  showSnachBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }
}
