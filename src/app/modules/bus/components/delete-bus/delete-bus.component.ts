import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-delete-bus',
  templateUrl: './delete-bus.component.html',
  styleUrls: ['./delete-bus.component.scss']
})
export class DeleteBusComponent {
  @Input() id!: number;
  constructor(private busService: BusService, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<DeleteBusComponent>){}
  
  cancel(){
    this.dialogRef.close(false);
  }

  delete(){
    this.busService.deleteBus(this.id).subscribe({
      next: (res) => {
        this.dialogRef.close(true);
      },
      error: (err) => {
        this.snackBar.open("Ocurrio un error al eliminar un bus!", "Cerrar", {duration: 3000})
      }
    });
  }
}
