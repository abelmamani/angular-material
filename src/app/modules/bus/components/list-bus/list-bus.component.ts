import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Bus } from 'src/app/models/bus.model';
import { BusService } from 'src/app/services/bus.service';
import { CreateBusComponent } from '../create-bus/create-bus.component';
import { UpdateBusComponent } from '../update-bus/update-bus.component';
import { DeleteBusComponent } from '../delete-bus/delete-bus.component';

@Component({
  selector: 'app-list-bus',
  templateUrl: './list-bus.component.html',
  styleUrls: ['./list-bus.component.scss']
})
export class ListBusComponent implements OnInit{
  buses: Bus[] = [];
  constructor(private busService: BusService, private snackBar: MatSnackBar, private dialog: MatDialog){}
  ngOnInit(): void {
    this.getBuses();
  }
  getBuses(){
    this.busService.getBuses().subscribe({
      next: (res: Bus[]) => {
        this.buses = res;
      },
      error: (err) => {
        this.showSnackBar("Ocurrio un problema al mostrar listado de buses!");
      }
    });
  }

  showSnackBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }

  openCreateBusDialg(){
    const dialogRef = this.dialog.open(CreateBusComponent);
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBuses();
        this.showSnackBar("Se añadio nuevo Bus!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }

  
  openUpdateBusDialg(id: number){
    const dialogRef = this.dialog.open(UpdateBusComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBuses();
        this.showSnackBar("Se actualizo Bus!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }

  
  openDeleteBusDialg(id: number){
    const dialogRef = this.dialog.open(DeleteBusComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBuses();
        this.showSnackBar("Se elimino Bus!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }
}
