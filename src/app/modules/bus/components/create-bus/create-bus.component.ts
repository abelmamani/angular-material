import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-create-bus',
  templateUrl: './create-bus.component.html',
  styleUrls: ['./create-bus.component.scss']
})
export class CreateBusComponent {
  formBus!: FormGroup;
  constructor(private busService: BusService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<CreateBusComponent>){
    this.formBus = this.formBuilder.group({
      id: [0, Validators.required],
      licensePlate: [undefined, Validators.required],
      brand: [undefined, Validators.required],
      model: [undefined, Validators.required],
      seats: [undefined, Validators.required]
    });
  }

  onSubmit(){
    if(this.formBus.valid){
      this.busService.createBus(this.formBus.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnachBar(err.error ? err.error.message : "ocurrio un problema al registrar un bus!");
        }
      });
    }else{
      this.showSnachBar("complete los campos!");
    }
  }
  showSnachBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }
}
