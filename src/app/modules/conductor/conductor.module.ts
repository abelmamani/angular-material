import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CreateConductorComponent } from './components/create-conductor/create-conductor.component';
import { DeleteConductorComponent } from './components/delete-conductor/delete-conductor.component';
import { ListConductorComponent } from './components/list-conductor/list-conductor.component';
import { UpdateConductorComponent } from './components/update-conductor/update-conductor.component';
import { ConductorRoutingModule } from './conductor-routing.module';
import { ConductorComponent } from './conductor.component';



@NgModule({
  declarations: [
    ConductorComponent,
    CreateConductorComponent,
    UpdateConductorComponent,
    DeleteConductorComponent,
    ListConductorComponent
  ],
  imports: [
    CommonModule,
    ConductorRoutingModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule
  ]
})
export class ConductorModule { }
