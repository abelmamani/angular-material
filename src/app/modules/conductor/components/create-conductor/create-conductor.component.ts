import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConductorService } from 'src/app/services/conductor.service';

@Component({
  selector: 'app-create-conductor',
  templateUrl: './create-conductor.component.html',
  styleUrls: ['./create-conductor.component.scss']
})
export class CreateConductorComponent {
  conductorForm!: FormGroup;
  constructor(private conductorService: ConductorService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<CreateConductorComponent>){
    this.conductorForm = this.formBuilder.group({
      id: 0,
      name: [undefined, Validators.required],
      lastName: [undefined, Validators.required],
      hireDate: [new Date(), Validators.required]
    });
  }

  onSubmit(){
    if(this.conductorForm.valid){
      this.conductorService.createConductor(this.conductorForm.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnachBar("hubo un problema al registrar!");
        }
      });
    }else{
      this.showSnachBar("completar los datos!");
    }
  }

  showSnachBar(msg: string){
    this.snackBar.open(msg, "cerrar", {duration: 3000});
  }
}
