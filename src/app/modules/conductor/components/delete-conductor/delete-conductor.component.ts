import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConductorService } from 'src/app/services/conductor.service';

@Component({
  selector: 'app-delete-conductor',
  templateUrl: './delete-conductor.component.html',
  styleUrls: ['./delete-conductor.component.scss']
})
export class DeleteConductorComponent {
  @Input() id!: number;
  constructor(private conductorSerice: ConductorService, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<DeleteConductorComponent>){}

  delete(){
    this.conductorSerice.deleteConductor(this.id).subscribe({
      next: (res) => {
        this.dialogRef.close(true);
      },
      error: (err) => {
        this.snackBar.open("Error al eliminar!", "Cerrar", {duration: 3000});
      }
    });
  }

  cancel(){
    this.dialogRef.close(false);
  }
}
