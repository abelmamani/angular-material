import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Conductor } from 'src/app/models/conductor.model';
import { ConductorService } from 'src/app/services/conductor.service';
import { CreateConductorComponent } from '../create-conductor/create-conductor.component';
import { UpdateConductorComponent } from '../update-conductor/update-conductor.component';
import { DeleteConductorComponent } from '../delete-conductor/delete-conductor.component';
@Component({
  selector: 'app-list-conductor',
  templateUrl: './list-conductor.component.html',
  styleUrls: ['./list-conductor.component.scss']
})
export class ListConductorComponent implements OnInit{
  conductors: Conductor[] = [];
  
  constructor(private conductorService: ConductorService, private snackBar: MatSnackBar, private dialog: MatDialog){}
  
  ngOnInit(): void {
    this.getConductors();
  }
  getConductors(){
    this.conductorService.getConductors().subscribe({
      next: (res: Conductor[]) => {
        this.conductors = res;
      },
      error: (err) => {
        this.showSnackBar("errror al obtener el listado!");
      }
    });
  }

  openCreateDialog(){
    const dialodRef = this.dialog.open(CreateConductorComponent);
    dialodRef.afterClosed().subscribe( result => {
      if(result){
        this.getConductors();
        this.showSnackBar("Se registro nuvo conductor!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }
  
  openUpdateDialog(id: number){
    const dialodRef = this.dialog.open(UpdateConductorComponent);
    dialodRef.componentInstance.id = id;
    dialodRef.afterClosed().subscribe( result => {
      if(result){
        this.getConductors();
        this.showSnackBar("Se actualizo conductor!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }

  openDeleteDialog(id: number){
    const dialodRef = this.dialog.open(DeleteConductorComponent);
    dialodRef.componentInstance.id = id;
    dialodRef.afterClosed().subscribe( result => {
      if(result){
          this.getConductors();
          this.showSnackBar("Se elimino conductor!");
      }else{
          this.showSnackBar("accion cancelada!");
      }
    });
  }

  showSnackBar(msg: string){
    this.snackBar.open(msg, 'cerrar', {duration: 3000});
  }
}
