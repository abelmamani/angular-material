import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Conductor } from 'src/app/models/conductor.model';
import { ConductorService } from 'src/app/services/conductor.service';

@Component({
  selector: 'app-update-conductor',
  templateUrl: './update-conductor.component.html',
  styleUrls: ['./update-conductor.component.scss']
})
export class UpdateConductorComponent implements OnInit{
  @Input() id! : number;
  conductorForm!: FormGroup;
  constructor(private conductorService: ConductorService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<UpdateConductorComponent>){
    this.conductorForm = this.formBuilder.group({
      id: 0,
      name: [undefined, Validators.required],
      lastName: [undefined, Validators.required],
      hireDate: [new Date(), Validators.required]
    });
  }

  ngOnInit(): void {
    this.conductorService.getConductor(this.id).subscribe({
      next: (res: Conductor) => {
        this.conductorForm.patchValue(res);
      },
      error: (err) => {
        this.showSnachBar("error al obtener el conductor!");
      }
    });
  }
  onSubmit(){
    if(this.conductorForm.valid){
      this.conductorService.updateConductor(this.conductorForm.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnachBar("hubo un problema al actualizar!");
        }
      });
    }else{
      this.showSnachBar("completar los datos!");
    }
  }

  showSnachBar(msg: string){
    this.snackBar.open(msg, "cerrar", {duration: 3000});
  }
}
