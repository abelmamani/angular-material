import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusStopService } from 'src/app/services/bus-stop.service';

@Component({
  selector: 'app-delete-bus-stop',
  templateUrl: './delete-bus-stop.component.html',
  styleUrls: ['./delete-bus-stop.component.scss']
})
export class DeleteBusStopComponent {
  @Input() id!: number;
  
  constructor(private busStopService: BusStopService, private dialogRef: MatDialogRef<DeleteBusStopComponent>, private snackBar: MatSnackBar){}
  
  cancel(){
    this.dialogRef.close(false);
  }

  delete(){
    this.busStopService.deleteBusStop(this.id).subscribe({
      next: (res) => {
        this.dialogRef.close(true);
      },
      error: (err) => {
        this.snackBar.open("Ocurrio un error al eliminar!", "Cerrar", {duration: 3000});
      }
    });
  }
}
