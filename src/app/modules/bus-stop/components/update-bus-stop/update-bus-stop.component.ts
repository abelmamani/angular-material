import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusStop } from 'src/app/models/bus.stop.model';
import { BusStopService } from 'src/app/services/bus-stop.service';

@Component({
  selector: 'app-update-bus-stop',
  templateUrl: './update-bus-stop.component.html',
  styleUrls: ['./update-bus-stop.component.scss']
})
export class UpdateBusStopComponent implements OnInit{
  @Input() id!: number;
  formBusStop!: FormGroup;
  constructor(private busStopService: BusStopService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<UpdateBusStopComponent>){
    this.formBusStop = this.formBuilder.group({
      id: [0, Validators.required],
      name: [undefined, Validators.required],
      description: [undefined, Validators.required],
      latitude: [undefined, Validators.required],
      longitude: [undefined, Validators.required],
    });
  }

  ngOnInit(): void {
    this.busStopService.getBusStop(this.id).subscribe({
      next: (res: BusStop) => {
        this.formBusStop.patchValue(res);
      },
      error: (err) => {
        this.showSnackBar("ocurrio un error al obtener el bus stop!");
      }
    });
  }

  onSubmit(){
    if(this.formBusStop.valid){
      this.busStopService.updateBusStop(this.formBusStop.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnackBar(err.error ? err.error.message : "ocurrio un error al actualizar la parada!")
        }
      });
    }else{
      this.showSnackBar("complete los campos!");
    }
  }

  showSnackBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }
}
