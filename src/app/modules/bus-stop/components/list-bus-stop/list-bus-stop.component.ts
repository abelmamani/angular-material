import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusStop } from 'src/app/models/bus.stop.model';
import { BusStopService } from 'src/app/services/bus-stop.service';
import { CreateBusStopComponent } from '../create-bus-stop/create-bus-stop.component';
import { UpdateBusStopComponent } from '../update-bus-stop/update-bus-stop.component';
import { DeleteBusStopComponent } from '../delete-bus-stop/delete-bus-stop.component';

@Component({
  selector: 'app-list-bus-stop',
  templateUrl: './list-bus-stop.component.html',
  styleUrls: ['./list-bus-stop.component.scss']
})
export class ListBusStopComponent implements OnInit{
  busStops: BusStop[] = [];
  constructor(private busStopService: BusStopService, private snackBar: MatSnackBar, private dialog: MatDialog){}
  ngOnInit(): void {
    this.getBusStops();  
  }


  getBusStops(){
    this.busStopService.getBusStops().subscribe({
      next: (res: BusStop[]) => {
        this.busStops = res;
      }, 
      error: (err) => {
        this.showSnackBar("no se puedo cargar las paradas!");
      }
    });
  }

  showSnackBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }

  openCreateDialog(){
    const dialogRef = this.dialog.open(CreateBusStopComponent);
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBusStops();
        this.showSnackBar("se creo nueva parada!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }

  openUpdateDialog(id: number){
    const dialogRef = this.dialog.open(UpdateBusStopComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBusStops();
        this.showSnackBar("se actualizo la parada!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }

  openDeleteDialog(id: number){
    const dialogRef = this.dialog.open(DeleteBusStopComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getBusStops();
        this.showSnackBar("se elimino la parada!");
      }else{
        this.showSnackBar("accion cancelada!");
      }
    });
  }
}
