import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BusStopService } from 'src/app/services/bus-stop.service';

@Component({
  selector: 'app-create-bus-stop',
  templateUrl: './create-bus-stop.component.html',
  styleUrls: ['./create-bus-stop.component.scss']
})
export class CreateBusStopComponent {
  formBusStop!: FormGroup;
  constructor(private busStopService: BusStopService, private snackBar: MatSnackBar, private formBuilder: FormBuilder, private dialogRef: MatDialogRef<CreateBusStopComponent>){
    this.formBusStop = this.formBuilder.group({
      id: [0, Validators.required],
      name: [undefined, Validators.required],
      description: [undefined, Validators.required],
      latitude: [undefined, Validators.required],
      longitude: [undefined, Validators.required],
    });
  }

  onSubmit(){
    if(this.formBusStop.valid){
      this.busStopService.createBusStop(this.formBusStop.value).subscribe({
        next: (res) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnackBar(err.error ? err.error.message : "ocurrio un error al registrar la parada!")
        }
      });
    }else{
      this.showSnackBar("complete los campos!");
    }
  }

  showSnackBar(msg: string){
    this.snackBar.open(msg, "Cerrar", {duration: 3000});
  }
}
