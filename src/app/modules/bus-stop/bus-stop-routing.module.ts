import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusStopComponent } from './bus-stop.component';

const routes: Routes = [
  {path: '', component: BusStopComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusStopRoutingModule { }
