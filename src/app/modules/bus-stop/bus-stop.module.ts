import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteBusStopComponent } from './components/delete-bus-stop/delete-bus-stop.component';
import { UpdateBusStopComponent } from './components/update-bus-stop/update-bus-stop.component';
import { CreateBusStopComponent } from './components/create-bus-stop/create-bus-stop.component';
import { ListBusStopComponent } from './components/list-bus-stop/list-bus-stop.component';
import { BusStopComponent } from './bus-stop.component';
import { BusStopRoutingModule } from './bus-stop-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';



@NgModule({
  declarations: [
    DeleteBusStopComponent,
    UpdateBusStopComponent,
    CreateBusStopComponent,
    ListBusStopComponent,
    BusStopComponent
  ],
  imports: [
    CommonModule,
    BusStopRoutingModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    MatSnackBarModule,
  ]
})
export class BusStopModule { }
