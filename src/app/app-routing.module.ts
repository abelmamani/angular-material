import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'conductor', pathMatch: 'full'},
  {path: 'conductor', loadChildren: () => import('./modules/conductor/conductor.module').then(m => m.ConductorModule)},
  {path: 'bus', loadChildren: () => import('./modules/bus/bus.module').then(m => m.BusModule)},
  {path: 'bus_stop', loadChildren: () => import('./modules/bus-stop/bus-stop.module').then(m => m.BusStopModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
