export interface BusStop{
    id: number,
    name: string,
    description: string,
    latitude: string,
    longitude: string
}