export interface Conductor {
    id: number,
    name: string,
    lastName: string,
    hireDate: Date
}